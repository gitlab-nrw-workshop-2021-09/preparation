* Start
    * [Startseite](README.md)
    * [Meine Online Vorbereitung :arrow_upper_right:](https://gitlab.com/gitlab-nrw-workshop-2021-09/preparation/-/issues?search=Online+Vorbereitung)
* Online-Vorbereitung
    * [Installation von Git](gitlab-install.md)
    * [Git Basics Versionskontrolle](git-basics.md)
    * Zusammenarbeit mit GitLab
        * [Projekte anlegen und konfigurieren](projects.md)
        * [Dokumentation, Markdown und Wikis](documentation.md)
        * [Projekte und Arbeitspakete planen und verfolgen](issues.md)
        * [Codeschnipsel](snippets.md)
