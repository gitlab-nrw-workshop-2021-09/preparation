# Installation von Git

* [Installation, Mac OSX (2:49)](https://www.youtube.com/watch?v=TvrZw47e7tI)
* [Installation, Windows (1:53)](https://www.youtube.com/watch?v=5KFn0r2XrtA)
* [Installationsanleitung, Windows](install_windows.md)
* [Installation, Linux (2:13)](https://www.youtube.com/watch?v=HSOuBmiCdrM)
* [Konfigurieren (2:46)](https://www.youtube.com/watch?v=PegV5zz5iFU)
* [Repository Anlegen, Mac OSX (2:51)](https://www.youtube.com/watch?v=Bo-pKqHO2go)
* [Repository Anlegen, Windows (6:34)](https://www.youtube.com/watch?v=8Qau5_NmF9s)
