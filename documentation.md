# Dokumentation, Markdown und Wikis

Jedes Projekt hat einen separaten Bereich für Dokumentation: das Wiki. Es ist standardmäßig für alle neuen Projekte aktiviert und befindet sich in Ihrem Projekt unter Wiki.

## Bearbeiten von Dokumentation in Wikis

Wikis sind sehr praktisch, wenn Sie Ihre Dokumentation nicht in Ihrem Repository, aber im selben Projekt aufbewahren wollen, in dem sich Ihre Daten befinden.

### Erstmalige Erstellung der Startseite

Wenn Sie zum ersten Mal ein Wiki besuchen, werden Sie aufgefordert, die Startseite zu erstellen.

Die Homepage ist notwendig, da sie als Startseite beim Betrachten eines Wikis dient. Sie müssen nur den Inhaltsbereich ausfüllen und auf Seite erstellen klicken. Sie können es später jederzeit bearbeiten, schreiben Sie zum Beispiel eine Willkommensnachricht.

### Erstellen einer neuen Wiki-Seite

Erstellen Sie eine neue Seite, indem Sie auf die Schaltfläche Neue Seite klicken, die sich auf allen Wiki-Seiten befindet. Sie werden aufgefordert, den Seitennamen einzugeben, aus dem GitLab den Pfad zur Seite erstellt. Sie können einen vollständigen Pfad für die neue Datei angeben und alle fehlenden Verzeichnisse werden automatisch erstellt.

Sobald Sie den Seitennamen eingegeben haben, können Sie den Inhalt eingeben. GitLab Wikis unterstützen Markdown, RDoc und AsciiDoc. Für Markdown-basierte Seiten werden alle Markdown-Funktionen unterstützt und für Links gibt es ein wiki-spezifisches Verhalten. In der Weboberfläche ist die Commit-Nachricht optional, aber das GitLab Wiki basiert auf Git und benötigt eine Commit-Nachricht, so dass eine für Sie erstellt wird, wenn Sie keine eingeben. 

Wenn Sie bereit sind, klicken Sie auf die Create Page und die neue Seite wird erstellt.

### Bearbeiten einer Wiki-Seite

Um eine Seite zu bearbeiten, klicken Sie einfach auf die Schaltfläche Edit. Wenn Sie fertig sind, klicken Sie auf Änderungen speichern, damit die Änderungen übernommen werden können.

### Löschen einer Wiki-Seite

Sie finden die Schaltfläche Delete nur, wenn Sie eine Seite bearbeiten. Klicken Sie darauf und bestätigen Sie, dass Sie die Seite löschen möchten.

## Formatierung mit Markdown

Markdown ist ein einfaches Textformat zum Schreiben strukturierter Dokumente, das auf Konventionen für die Angabe der Formatierung in E-Mail- und Usenet-Posts basiert. GitLab verwendet Markdown an vielen Stellen, um Text einfach formatieren zu können. Eine vollständige Liste der Formatierungen finden Sie in der GitLab Hilfe zu Markdown. Im folgenden finden Sie die wichtigsten Elemente:

### Überschriften
Um eine Überschrift hinzuzufügen, können Sie ein bis sechs #-Symbole benutzen. Je mehr Symbole, desto kleiner wird die Überschrift. Zum Beispiel wird

```markdown
# Wichtiges Thema
## Unterthema
###### Ganz kleine Überschrift
```
so dargestellt:

# Wichtiges Thema
## Unterthema
###### Ganz kleine Überschrift

### Text

Sie können Textabschnitte mit verschiedenen Stilen formatieren, indem der Text mit den Zeichen ```*``` oder ```_``` eingeschlossen wird:


| Stil   | Syntax                       | Example                       | Output |
|--------|------------------------------|-------------------------------|--------|
| Fett   | `** **` oder `__ __` |	`**This is bold text**`	| **This is bold text* |
| Kursiv |	`* *` oder ```_ _`    | `*This text is italicized*` | *This text is italicized* |
| Durchgestrichen |	`~~ ~~`	            | `~~This was mistaken text~~`    | ~~This was mistaken text~~ | 
| Fett und Kursiv | `** **` und `_ _`	    | `**This text is _extremely_ important**` | **This text is _extremely_ important** |

### Links

Korrekt formatierte URLs werden automatisch erkannt. Wenn Sie eine andere Beschriftung benötigen, können Sie den Link Text in eckigen Klammern [ ] und die URL in runde Klammern ( ) fassen. Zum Beispiel wird

```markdown
Ein Link auf [GitLab](https://git.rwth-aachen.de)
```

dargestellt als

Ein Link auf [GitLab](https://git.rwth-aachen.de)

### Listen
Zeilen, die mit `-`, `*` oder `1.` beginnen, werden als Liste dargestellt. So wird 

```markdown
* Das ist
* Ein Test
```
zu

* Das ist
* Ein Test

und 

```markdown
1. Diese Liste
1. hat eine
1. Reihenfolge
```

wird als

1. Diese Liste
1. hat eine
1. Reihenfolge

dargestellt.

### Zitate
Um ein Textzitat zu kennzeichnen, können Sie das Zeichen > verwenden. So wird

```markdown
In der Spezifikation steht
> genau so muss es sein
dargestellt als
```

In der Spezifikation steht
> genau so muss es sein

Sie können auch Quellcode zitieren, indem Sie ` ``` ` verwenden. So wird 

````markdown
```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```
````

Dargestellt als

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```
