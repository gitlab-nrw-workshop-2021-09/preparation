# Git Installationsanleitung, Windows

Im Folgenden wird die Installation von Git (notwendig) und tortoiseGit (optional) beschrieben. Sollten Sie Probleme oder Fragen zu der Installation haben oder es zu unerwarteten Problemen kommen, melden Sie sich bitte im Forum dieses Kurses.

## Git

Die Installationsdatei können Sie unter folgender URL herunterladen: https://git-scm.com

![](./img/1_Download.png)

Klicken Sie auf dieser Seite entweder direkt auf den Download-Link auf der rechten Seite unterhalb der aktuellen Release Version oder ...

![](./img/2_Download.png)

... wechseln Sie per Klick auf "Downloads" in den Downloadbereich und wählen dort den passenden Download aus.

![](./img/3_Download.png)

Bestätigen Sie anschließend, dass Sie die Datei speichern möchten. Nach Abschluss des Downloads führen Sie diesen bitte per Doppelklick auf die Datei aus. Nach Start der Installationsroutine müssen Sie bestätigen, dass dieses Programm Änderungen an Ihrem PC vornimmt, anschließend öffnet sich das Installations-Programm.

![](./img/4_Install_cut.png)

Bestätigen Sie die Lizenzinformationen über den Button "Next".

![](./img/5_Install_cut.png)

Auf der folgenden Seite wählen Sie bitte ein Installationsverzeichnis für git aus und bestätigen dies über den Button "Next".

![](./img/6_install_cut.png)

Lassen Sie die ausgewählten Komponenten so, wie sie voreingestellt sind und bestätigen Sie über den Button "Next".

![](./img/7_install_cut.png)

Wählen Sei einen Namen für den Menüeintrag im Startmenü (im Normalfall kann man den voreingestellten Namen verwenden) und bestätigen Sie mit dem Button "Next".

![](./img/8_install_cut.png)

Wählen Sie nun den Texteditor, den git Standarmäßg verwenden soll. Als Empfehlung sollte vim ausgewählt werden, mit dem Hinweis, dass dieser Texteditor eventuell eine kurze Einarbeitung erforderlich macht. Eine Übersicht über die Bedienung von vim und vorhandene Befehle bieten z.B. https://code-bude.net/2013/03/01/vim-einsteiger-tutorial/ oder https://www.grund-wissen.de/linux/shell/vim/bedienung.html.

![](./img/9_install_cut.png)

Wählen Sie auf der nächsten Seite den Punkt „Use Git from the Windows command prompt“ (voreingestellt) und bestätigen Sie mit dem Button "Next".

![](./img/10_install_cut.png)

Übernehmen Sie auch auf der folgenden Seite die voreinstellten Werte und bestätigen Sie über den Button "Next".

![](./img/11_install_cut.png)

Auch auf dieser Seite übernehmen Sie den voreingestellten Wert und fahren über "Next" mit der Installation fort.

![](./img/12_install_cut.png)

Wählen Sie auf der folgenden Seiten den obersten, voreingestellten Punkt und setzen Sie die Installation mit "Next" fort.

![](./img/13_install_cut.png)

Wählen Sie bitte den obersten Punkt, falls er nicht vorausgewählt sein sollte, und fahren Sie mit "Next" fort.

![](./img/14_install_cut.png)

Behalten Sie auch im nächsten Schritt die voreingestellten Werte und setzen Sie die Installation mit "Next" fort.

![](./img/15_install_cut.png)

Lassen Sie die Features im nächsten Schritt abgewählt und setzten Sie die Installation fort.

![](./img/16_install_cut.png)

Warten Sie bis die Installation abgeschlossen ist.

![](./img/17_install_cut.png)

Beenden Sie den Installer über "Finish".

![](./img/18_StartMenue_cut.png)

Im Startmenü finden Sie das neu installierte Git.

![](./img/19_Kontextmenue_cut.png)

Auch im Kontextmenü (rechter Mausklick) finden Sie nun Befehle, um Git zu starten.

Die haben Git erfolgreich installiert!

## TortoiseGit

Die Installation von tortoiseGit ist optional. Um tortoiseGit installieren und verwenden zu können, benötigen Sie eine vorhandene Git Installation. Die benötigte Installationsdatei können Sie sich auf folgender Website herunterladen: https://tortoisegit.org

![](./img/1_Download_tort.jpg)

Klicken Sie auf dieser Seite auf "Download" ...

![](./img/2_Download_tort.png)

... um in den Downloadbereich zu gelangen. Wählen Sie hier die für Ihr System benötigte Datei und klicken Sie auf den entsprechenden Link.

![](./img/3_Download_tort.png)

Speichern Sie die Datei auf Ihrem Rechner.

![](./img/4_Install_tort.png)

Nach Abschluss des Downloads starten Sie die Datei per Doppelklick und bestätigen den folgenden Dialog mit "OK".

![](./img/5_Install_tort.png)

Die Installationsroutine sollte nun starten, setzen Sie sie über den Button "Next" fort.

![](./img/6_install_tort.png)

Lesen Sie die Lizenzinformationen und klicken Sie "Next".

![](./img/7_install_tort.png)

Wählen Sie in diesem Dialog den obersten Auswahlpunkt und bestätigen Sie mit "Next".

![](./img/8_install_tort.png)

Wählen Sie einen Installationsort für tortoiseGit auf Ihrer Festplatte.

![](./img/9_install_tort.png)

Starten Sie anschließend die Installation über den Button "Install".

![](./img/10_install_tort.png)

Warten Sie bis die Installation abgeschlossen ist.

![](./img/11_install_tort.png)

Setzen Sie den Haken in der Auswahlbox wie gezeigt und klicken Sie "Finish".

![](./img/12_install_tort.png)

Wählen Sie die Sprache aus, in der tortoiseGit laufen soll. Wir empfehlen Ihnen Englisch zu verwenden.

![](./img/13_install_tort.png)

Setzen Sie die Ersteinrichtung mit "Weiter" fort.

![](./img/14_install_tort.png)

Der anzugebende Pfad zu git.ext sollte automatisch eingetragen werden. Andernfalls finden Sie eine git.ext in Ihrem Git Installationsverzeichnis. Klicken Sie "Weiter".

![](./img/15_install_tort.png)

Wählen Sie auf dieser Seite "Don't store these settings now" und klicken Sie auf "Weiter".

![](./img/17_install_tort.png)

Wählen Sie auch hier "Don't store these settings now" und bestätigen Sie mit "Fertigstellen".

![](./img/18_Startmenue_tort.png)

TortoiseGit ist installiert. Sie finden es z.B. im Startmenü.
